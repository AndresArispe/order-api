package com.milankas.training.orderapi.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.milankas.training.orderapi.dto.in.AddressDto;
import com.milankas.training.orderapi.dto.in.LineItemDto;
import com.milankas.training.orderapi.dto.in.OrderDto;
import com.milankas.training.orderapi.dto.in.patch.OrderPatchDto;
import com.milankas.training.orderapi.dto.out.OrderOutDto;
import com.milankas.training.orderapi.persistance.model.Address;
import com.milankas.training.orderapi.services.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(MockitoExtension.class)
class OrderControllerIntegrationTest {

    @Mock
    OrderService orderService;

    @Mock
    RabbitTemplate template;

    @InjectMocks
    OrderController orderController;

    MockMvc mockMvc;

    OrderOutDto orderOutDto = new OrderOutDto();

    UUID orderId;
    UUID orderId1;
    UUID userId;
    String emailAddres;

    AddressDto addressDto = new AddressDto();
    String addressLine1;
    String addressLine2;
    String contactName;
    String contactPhoneNumber;
    String state;
    String city;
    String zipCode;
    String countryCode;

    LineItemDto lineItemDto = new LineItemDto();
    UUID productId;
    int quantity;


    @BeforeEach
    void setUp() {
        orderId = UUID.fromString("8d6b895f-b885-4e15-894f-a0857b513f55");
        orderId1 = UUID.fromString("a791b7c7-baa9-4a0f-b3e9-ca024fd54478");
        userId = UUID.fromString("a845b9c7-baa9-4a0f-b3e9-ca024fd54478");
        emailAddres = "order@gmail.com";

        addressLine1 = "address1";
        addressLine2 = "address2";
        contactName = "contact1";
        contactPhoneNumber = "16443215";
        state = "state1";
        city = "city1";
        zipCode = "00000";
        countryCode = "TO";

        addressDto.setAddressLine1(addressLine1);
        addressDto.setAddressLine2(addressLine2);
        addressDto.setContactName(contactName);
        addressDto.setContactPhoneNumber(contactPhoneNumber);
        addressDto.setState(state);
        addressDto.setCity(city);
        addressDto.setZipCode(zipCode);
        addressDto.setCountryCode(countryCode);

        productId = UUID.fromString("a845b9c7-baa9-4a0f-b3e9-ca056fd30478");
        quantity = 20;

        lineItemDto.setProductId(productId);
        lineItemDto.setQty(quantity);

        mockMvc = MockMvcBuilders
                .standaloneSetup(orderController)
                .build();
    }

    @Test
    void getAllOrders() throws Exception {
        List<OrderOutDto> orderOutDtoList = new ArrayList<>();
        orderOutDto.setId(orderId);
        orderOutDto.setUserId(userId);
        orderOutDto.setEmailAddress(emailAddres);
        orderOutDto.setAddress(addressDto);
        List<LineItemDto> lineItemDtos = new ArrayList<>();
        lineItemDtos.add(lineItemDto);
        orderOutDto.setLineItems(lineItemDtos);
        orderOutDtoList.add(orderOutDto);
        when(orderService.getAllOrders()).thenReturn(orderOutDtoList);

        MvcResult result = mockMvc.perform(get("/v1/orders"))
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        assertEquals(content,"[{\"id\":\"8d6b895f-b885-4e15-894f-a0857b513f55\",\"userId\":\"a845b9c7-baa9-4a0f-b3e9-ca024fd54478\",\"emailAddress\":\"order@gmail.com\",\"address\":{\"addressLine1\":\"address1\",\"addressLine2\":\"address2\",\"contactName\":\"contact1\",\"contactPhoneNumber\":\"16443215\",\"state\":\"state1\",\"city\":\"city1\",\"zipCode\":\"00000\",\"countryCode\":\"TO\"},\"lineItems\":[{\"productId\":\"a845b9c7-baa9-4a0f-b3e9-ca056fd30478\",\"qty\":20}]}]");
        verify(orderService, times(1)).getAllOrders();
    }

    @Test
    void getOrder() throws Exception {
        orderOutDto.setId(orderId);
        orderOutDto.setUserId(userId);
        orderOutDto.setEmailAddress(emailAddres);
        orderOutDto.setAddress(addressDto);
        List<LineItemDto> lineItemDtos = new ArrayList<>();
        lineItemDtos.add(lineItemDto);
        orderOutDto.setLineItems(lineItemDtos);
        when(orderService.getOrderById(any())).thenReturn(orderOutDto);

        MvcResult result = mockMvc.perform(get("/v1/orders/8d6b895f-b885-4e15-894f-a0857b513f55"))
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        assertEquals("{\"id\":\"8d6b895f-b885-4e15-894f-a0857b513f55\",\"userId\":\"a845b9c7-baa9-4a0f-b3e9-ca024fd54478\",\"emailAddress\":\"order@gmail.com\",\"address\":{\"addressLine1\":\"address1\",\"addressLine2\":\"address2\",\"contactName\":\"contact1\",\"contactPhoneNumber\":\"16443215\",\"state\":\"state1\",\"city\":\"city1\",\"zipCode\":\"00000\",\"countryCode\":\"TO\"},\"lineItems\":[{\"productId\":\"a845b9c7-baa9-4a0f-b3e9-ca056fd30478\",\"qty\":20}]}",content);
        verify(orderService,times(2)).getOrderById(any());
    }

    @Test
    void healthCheck() throws Exception {
        MvcResult result = mockMvc.perform(get("/v1/orders/healthcheck"))
                .andExpect(status().isOk())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        assertEquals(content,"I'm Alive");
    }

    @Test
    void postOrder() throws Exception {
        UUID orderId = UUID.fromString("8d6b895f-b885-4e15-894f-a0857b513f55");
        UUID userId = UUID.fromString("c3c2e1cc-b55d-43d8-b389-6da56bec950b");
        UUID productId = UUID.fromString("668e315d-89df-43fa-913b-87f67db18fb5");

        OrderDto orderDto = new OrderDto();
        orderDto.setUserId(userId);
        orderDto.setEmailAddress("company@gmail.com");
        AddressDto addressDto = new AddressDto();
        addressDto.setAddressLine1("addressTest1");
        addressDto.setAddressLine2("addressTest2");
        addressDto.setContactName("testName");
        addressDto.setContactPhoneNumber("316516783");
        addressDto.setState("state1");
        addressDto.setCity("city2");
        addressDto.setZipCode("00000");
        addressDto.setCountryCode("TO");

        orderDto.setAddress(addressDto);

        LineItemDto lineItem = new LineItemDto();
        List<LineItemDto> lineItems = new ArrayList<>();

        lineItem.setProductId(productId);
        lineItem.setQty(5);
        lineItems.add(lineItem);

        orderDto.setLineItems(lineItems);

        OrderOutDto orderOutDto = new OrderOutDto();
        orderOutDto.setId(orderId);

        when(orderService.saveOrder(any())).thenReturn(orderOutDto);

        MvcResult result = mockMvc.perform(post("/v1/orders")
                .content(asJsonString(orderDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        verify(orderService,times(1)).saveOrder(any());
        String content = result.getResponse().getContentAsString();
        assertEquals(content, "{\"id\":\"8d6b895f-b885-4e15-894f-a0857b513f55\",\"userId\":null,\"emailAddress\":null,\"address\":null,\"lineItems\":[]}");
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void deleteOrder() throws Exception {
        UUID orderId = UUID.fromString("8d6b895f-b885-4e15-894f-a0857b513f55");
        OrderOutDto orderOutDto = new OrderOutDto();
        orderOutDto.setId(orderId);
        when(orderService.deleteOrder(any())).thenReturn(orderOutDto);

        ResultActions result = mockMvc.perform(delete("/v1/orders/8d6b895f-b885-4e15-894f-a0857b513f55"))
                .andExpect(status().isNoContent());

        verify(orderService,times(1)).deleteOrder(any());
    }

    @Test
    void patchOrder() throws Exception {
        orderOutDto.setId(orderId);
        orderOutDto.setUserId(userId);
        orderOutDto.setEmailAddress(emailAddres);
        orderOutDto.setAddress(addressDto);
        List<LineItemDto> lineItemDtos = new ArrayList<>();
        lineItemDtos.add(lineItemDto);

        OrderPatchDto orderPatchDto = new OrderPatchDto();
        orderPatchDto.setUserId(userId);

        when(orderService.getOrderById(any())).thenReturn(orderOutDto);
        when(orderService.updateOrder(any(), any())).thenReturn(orderOutDto);

        MvcResult result = mockMvc.perform(patch("/v1/orders/8d6b895f-b885-4e15-894f-a0857b513f55")
                .content(asJsonString(orderPatchDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        verify(orderService, times(1)).getOrderById(any());
        verify(orderService, times(1)).updateOrder(any(),any());

        String content = result.getResponse().getContentAsString();
        assertEquals("{\"id\":\"8d6b895f-b885-4e15-894f-a0857b513f55\",\"userId\":\"a845b9c7-baa9-4a0f-b3e9-ca024fd54478\",\"emailAddress\":\"order@gmail.com\",\"address\":{\"addressLine1\":\"address1\",\"addressLine2\":\"address2\",\"contactName\":\"contact1\",\"contactPhoneNumber\":\"16443215\",\"state\":\"state1\",\"city\":\"city1\",\"zipCode\":\"00000\",\"countryCode\":\"TO\"},\"lineItems\":[]}", content);
    }
}