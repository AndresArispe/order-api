package com.milankas.training.orderapi.controllers;

import com.milankas.training.orderapi.dto.in.AddressDto;
import com.milankas.training.orderapi.dto.in.LineItemDto;
import com.milankas.training.orderapi.dto.in.OrderDto;
import com.milankas.training.orderapi.dto.in.patch.OrderPatchDto;
import com.milankas.training.orderapi.dto.out.OrderOutDto;
import com.milankas.training.orderapi.errors.ErrorResponse;
import com.milankas.training.orderapi.services.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.*;
import org.springframework.web.server.ResponseStatusException;

import java.beans.PropertyEditor;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrderControllerTest {

    @Mock
    OrderService orderService;

    @Mock
    RabbitTemplate template;

    @InjectMocks
    OrderController orderController;

    BindingResult resultWithErrors = new BindingResult() {
        @Override
        public Object getTarget() {
            return null;
        }

        @Override
        public Map<String, Object> getModel() {
            return null;
        }

        @Override
        public Object getRawFieldValue(String s) {
            return null;
        }

        @Override
        public PropertyEditor findEditor(String s, Class<?> aClass) {
            return null;
        }

        @Override
        public PropertyEditorRegistry getPropertyEditorRegistry() {
            return null;
        }

        @Override
        public String[] resolveMessageCodes(String s) {
            return new String[0];
        }

        @Override
        public String[] resolveMessageCodes(String s, String s1) {
            return new String[0];
        }

        @Override
        public void addError(ObjectError objectError) {

        }

        @Override
        public String getObjectName() {
            return null;
        }

        @Override
        public void setNestedPath(String s) {

        }

        @Override
        public String getNestedPath() {
            return null;
        }

        @Override
        public void pushNestedPath(String s) {

        }

        @Override
        public void popNestedPath() throws IllegalStateException {

        }

        @Override
        public void reject(String s) {

        }

        @Override
        public void reject(String s, String s1) {

        }

        @Override
        public void reject(String s, Object[] objects, String s1) {

        }

        @Override
        public void rejectValue(String s, String s1) {

        }

        @Override
        public void rejectValue(String s, String s1, String s2) {

        }

        @Override
        public void rejectValue(String s, String s1, Object[] objects, String s2) {

        }

        @Override
        public void addAllErrors(Errors errors) {

        }

        @Override
        public boolean hasErrors() {
            return true;
        }

        @Override
        public int getErrorCount() {
            return 0;
        }

        @Override
        public List<ObjectError> getAllErrors() {
            List<ObjectError> errors = new ArrayList<>();
            ObjectError error = new ObjectError("Bad Dto", "Validation Failure");
            errors.add(error);
            return errors;
        }

        @Override
        public boolean hasGlobalErrors() {
            return false;
        }

        @Override
        public int getGlobalErrorCount() {
            return 0;
        }

        @Override
        public List<ObjectError> getGlobalErrors() {
            return null;
        }

        @Override
        public ObjectError getGlobalError() {
            return null;
        }

        @Override
        public boolean hasFieldErrors() {
            return false;
        }

        @Override
        public int getFieldErrorCount() {
            return 0;
        }

        @Override
        public List<FieldError> getFieldErrors() {
            return null;
        }

        @Override
        public FieldError getFieldError() {
            return null;
        }

        @Override
        public boolean hasFieldErrors(String s) {
            return false;
        }

        @Override
        public int getFieldErrorCount(String s) {
            return 0;
        }

        @Override
        public List<FieldError> getFieldErrors(String s) {
            return null;
        }

        @Override
        public FieldError getFieldError(String s) {
            return null;
        }

        @Override
        public Object getFieldValue(String s) {
            return null;
        }

        @Override
        public Class<?> getFieldType(String s) {
            return null;
        }
    };

    BindingResult resultWithoutErrors = new BindingResult() {
        @Override
        public Object getTarget() {
            return null;
        }

        @Override
        public Map<String, Object> getModel() {
            return null;
        }

        @Override
        public Object getRawFieldValue(String s) {
            return null;
        }

        @Override
        public PropertyEditor findEditor(String s, Class<?> aClass) {
            return null;
        }

        @Override
        public PropertyEditorRegistry getPropertyEditorRegistry() {
            return null;
        }

        @Override
        public String[] resolveMessageCodes(String s) {
            return new String[0];
        }

        @Override
        public String[] resolveMessageCodes(String s, String s1) {
            return new String[0];
        }

        @Override
        public void addError(ObjectError objectError) {

        }

        @Override
        public String getObjectName() {
            return null;
        }

        @Override
        public void setNestedPath(String s) {

        }

        @Override
        public String getNestedPath() {
            return null;
        }

        @Override
        public void pushNestedPath(String s) {

        }

        @Override
        public void popNestedPath() throws IllegalStateException {

        }

        @Override
        public void reject(String s) {

        }

        @Override
        public void reject(String s, String s1) {

        }

        @Override
        public void reject(String s, Object[] objects, String s1) {

        }

        @Override
        public void rejectValue(String s, String s1) {

        }

        @Override
        public void rejectValue(String s, String s1, String s2) {

        }

        @Override
        public void rejectValue(String s, String s1, Object[] objects, String s2) {

        }

        @Override
        public void addAllErrors(Errors errors) {

        }

        @Override
        public boolean hasErrors() {
            return false;
        }

        @Override
        public int getErrorCount() {
            return 1;
        }

        @Override
        public List<ObjectError> getAllErrors() {
            return null;
        }

        @Override
        public boolean hasGlobalErrors() {
            return false;
        }

        @Override
        public int getGlobalErrorCount() {
            return 0;
        }

        @Override
        public List<ObjectError> getGlobalErrors() {
            return null;
        }

        @Override
        public ObjectError getGlobalError() {
            return null;
        }

        @Override
        public boolean hasFieldErrors() {
            return false;
        }

        @Override
        public int getFieldErrorCount() {
            return 0;
        }

        @Override
        public List<FieldError> getFieldErrors() {
            return null;
        }

        @Override
        public FieldError getFieldError() {
            return null;
        }

        @Override
        public boolean hasFieldErrors(String s) {
            return false;
        }

        @Override
        public int getFieldErrorCount(String s) {
            return 0;
        }

        @Override
        public List<FieldError> getFieldErrors(String s) {
            return null;
        }

        @Override
        public FieldError getFieldError(String s) {
            return null;
        }

        @Override
        public Object getFieldValue(String s) {
            return null;
        }

        @Override
        public Class<?> getFieldType(String s) {
            return null;
        }
    };

    OrderOutDto orderOutDto = new OrderOutDto();

    UUID orderId;
    UUID orderId1;
    UUID userId;
    String emailAddres;

    AddressDto addressDto = new AddressDto();
    String addressLine1;
    String addressLine2;
    String contactName;
    String contactPhoneNumber;
    String state;
    String city;
    String zipCode;
    String countryCode;

    LineItemDto lineItemDto = new LineItemDto();
    UUID productId;
    int quantity;


    @BeforeEach
    void setUp() {
        orderId = UUID.fromString("8d6b895f-b885-4e15-894f-a0857b513f55");
        orderId1 = UUID.fromString("a791b7c7-baa9-4a0f-b3e9-ca024fd54478");
        userId = UUID.fromString("a845b9c7-baa9-4a0f-b3e9-ca024fd54478");
        emailAddres = "order@gmail.com";

        addressLine1 = "address1";
        addressLine2 = "address2";
        contactName = "contact1";
        contactPhoneNumber = "16443215";
        state = "state1";
        city = "city1";
        zipCode = "00000";
        countryCode = "TO";

        addressDto.setAddressLine1(addressLine1);
        addressDto.setAddressLine2(addressLine2);
        addressDto.setContactName(contactName);
        addressDto.setContactPhoneNumber(contactPhoneNumber);
        addressDto.setState(state);
        addressDto.setCity(city);
        addressDto.setZipCode(zipCode);
        addressDto.setCountryCode(countryCode);

        productId = UUID.fromString("a845b9c7-baa9-4a0f-b3e9-ca056fd30478");
        quantity = 20;

        lineItemDto.setProductId(productId);
        lineItemDto.setQty(quantity);
    }

    @Test
    void getAllOrdersTest() {
        List<OrderOutDto> orderOutDtoList = new ArrayList<>();
        orderOutDto.setId(orderId);
        orderOutDto.setUserId(userId);
        orderOutDto.setEmailAddress(emailAddres);
        orderOutDto.setAddress(addressDto);
        List<LineItemDto> lineItemDtos = new ArrayList<>();
        lineItemDtos.add(lineItemDto);
        orderOutDto.setLineItems(lineItemDtos);
        orderOutDtoList.add(orderOutDto);

        when(orderService.getAllOrders()).thenReturn(orderOutDtoList);
        List<OrderOutDto> allOrders = orderController.getAllOrders();
        assertEquals(orderOutDtoList, allOrders);
    }

    @Test
    void getOrderTest() {
        orderOutDto.setId(orderId);
        orderOutDto.setUserId(userId);
        orderOutDto.setEmailAddress(emailAddres);
        orderOutDto.setAddress(addressDto);
        List<LineItemDto> lineItemDtos = new ArrayList<>();
        lineItemDtos.add(lineItemDto);
        orderOutDto.setLineItems(lineItemDtos);
        when(orderService.getOrderById(any())).thenReturn(orderOutDto);
        OrderOutDto order = orderController.getOrder(orderId);
        assertEquals(orderOutDto, order);
    }

    @Test
    void healthCheckTest() {
        String content = orderController.healthCheck();
        assertEquals("I'm Alive", content);
    }

    @Test
    void postOrderWithValidData() {
        orderOutDto.setId(orderId);
        orderOutDto.setUserId(userId);
        orderOutDto.setEmailAddress(emailAddres);
        orderOutDto.setAddress(addressDto);
        List<LineItemDto> lineItemDtos = new ArrayList<>();
        lineItemDtos.add(lineItemDto);
        orderOutDto.setLineItems(lineItemDtos);

        OrderDto orderDto = new OrderDto();
        orderDto.setUserId(userId);
        orderDto.setEmailAddress("company@gmail.com");
        AddressDto addressDto = new AddressDto();
        addressDto.setAddressLine1("addressTest1");
        addressDto.setAddressLine2("addressTest2");
        addressDto.setContactName("testName");
        addressDto.setContactPhoneNumber("316516783");
        addressDto.setState("state1");
        addressDto.setCity("city2");
        addressDto.setZipCode("00000");
        addressDto.setCountryCode("TO");

        orderDto.setAddress(addressDto);

        LineItemDto lineItem = new LineItemDto();
        List<LineItemDto> lineItems = new ArrayList<>();

        lineItem.setProductId(productId);
        lineItem.setQty(5);
        lineItems.add(lineItem);

        orderDto.setLineItems(lineItems);

        when(orderService.saveOrder(any())).thenReturn(orderOutDto);
        ResponseEntity<Object> order = orderController.postOrder(orderDto, resultWithoutErrors);
        ResponseEntity<Object> test = new ResponseEntity<>(orderOutDto, HttpStatus.CREATED);

        assertEquals(order.getStatusCodeValue(), 201);
        assertEquals(order, test);
    }

    @Test
    void postOrderWithNoValidData() {
        orderOutDto.setId(orderId);
        orderOutDto.setUserId(userId);
        orderOutDto.setEmailAddress(emailAddres);
        orderOutDto.setAddress(addressDto);
        List<LineItemDto> lineItemDtos = new ArrayList<>();
        lineItemDtos.add(lineItemDto);
        orderOutDto.setLineItems(lineItemDtos);

        OrderDto orderDto = new OrderDto();
        orderDto.setUserId(userId);
        orderDto.setEmailAddress("company@gmail.com");
        AddressDto addressDto = new AddressDto();
        addressDto.setAddressLine1("addressTest1");
        addressDto.setAddressLine2("addressTest2");
        addressDto.setContactName("testName");
        addressDto.setContactPhoneNumber("316516783");
        addressDto.setState("state1");
        addressDto.setCity("city2");
        addressDto.setZipCode("00000");
        addressDto.setCountryCode("TO");

        orderDto.setAddress(addressDto);

        LineItemDto lineItem = new LineItemDto();
        List<LineItemDto> lineItems = new ArrayList<>();

        lineItem.setProductId(productId);
        lineItem.setQty(5);
        lineItems.add(lineItem);

        orderDto.setLineItems(lineItems);

        List<String> messages = new ArrayList<>();
        messages.add("Validation Failure");

        ResponseEntity<Object> order = orderController.postOrder(orderDto, resultWithErrors);
        ResponseEntity<Object> test = new ResponseEntity<>(new ErrorResponse("400", "Validation Failure", messages), HttpStatus.BAD_REQUEST);

        assertEquals(400, order.getStatusCodeValue());

    }

    @Test
    void deleteOrderWithValidId() {
        orderOutDto.setId(orderId);
        orderOutDto.setUserId(userId);
        orderOutDto.setEmailAddress(emailAddres);
        orderOutDto.setAddress(addressDto);
        List<LineItemDto> lineItemDtos = new ArrayList<>();
        lineItemDtos.add(lineItemDto);
        orderOutDto.setLineItems(lineItemDtos);
        when(orderService.deleteOrder(any())).thenReturn(orderOutDto);
        orderController.deleteOrder(orderId);
        verify(orderService, times(1)).deleteOrder(any());
    }

    @Test
    void patchOrder() {
        orderOutDto.setId(orderId);
        orderOutDto.setUserId(userId);
        orderOutDto.setEmailAddress(emailAddres);
        orderOutDto.setAddress(addressDto);
        List<LineItemDto> lineItemDtos = new ArrayList<>();
        lineItemDtos.add(lineItemDto);

        OrderPatchDto orderPatchDto = new OrderPatchDto();
        orderPatchDto.setUserId(userId);

        when(orderService.getOrderById(any())).thenReturn(orderOutDto);
        when(orderService.updateOrder(any(), any())).thenReturn(orderOutDto);

        ResponseEntity<Object> order = orderController.patchOrder(orderId, orderPatchDto, resultWithoutErrors);
        ResponseEntity<Object> test = new ResponseEntity<>(orderOutDto, HttpStatus.OK);

        assertEquals(order, test);
    }

    @Test
    void patchOrderWithInvalidInput() {
        orderOutDto.setId(orderId);
        orderOutDto.setUserId(userId);
        orderOutDto.setEmailAddress(emailAddres);
        orderOutDto.setAddress(addressDto);
        List<LineItemDto> lineItemDtos = new ArrayList<>();
        lineItemDtos.add(lineItemDto);

        OrderPatchDto orderPatchDto = new OrderPatchDto();
        orderPatchDto.setUserId(userId);

        ResponseEntity<Object> order = orderController.patchOrder(orderId, orderPatchDto, resultWithErrors);
        ResponseEntity<Object> test = new ResponseEntity<>(orderOutDto, HttpStatus.OK);

        assertEquals(400, order.getStatusCodeValue());
    }
}