package com.milankas.training.orderapi.services;

import com.milankas.training.orderapi.dto.in.LineItemDto;
import com.milankas.training.orderapi.dto.in.OrderDto;
import com.milankas.training.orderapi.dto.in.patch.OrderPatchDto;
import com.milankas.training.orderapi.dto.out.OrderOutDto;
import com.milankas.training.orderapi.mappers.in.AddressMapper;
import com.milankas.training.orderapi.mappers.in.LineItemMapper;
import com.milankas.training.orderapi.mappers.in.OrderMapper;
import com.milankas.training.orderapi.mappers.in.patch.OrderPatchMapper;
import com.milankas.training.orderapi.mappers.out.OrderOutMapper;
import com.milankas.training.orderapi.persistance.model.Address;
import com.milankas.training.orderapi.persistance.model.LineItem;
import com.milankas.training.orderapi.persistance.model.Order;
import com.milankas.training.orderapi.persistance.repository.AddressRepository;
import com.milankas.training.orderapi.persistance.repository.LineItemRepository;
import com.milankas.training.orderapi.persistance.repository.OrderRepository;
import org.aspectj.weaver.ast.Or;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {


    @Mock
    AddressRepository addressRepository;

    @Mock
    LineItemRepository lineItemRepository;

    @Mock
    OrderRepository orderRepository;

    @Mock
    OrderMapper orderMapper;

    @Mock
    LineItemMapper lineItemMapper;

    @Mock
    AddressMapper addressMapper;

    @Mock
    OrderOutMapper orderOutMapper;

    @Mock
    OrderPatchMapper orderPatchMapper;

    @InjectMocks
    OrderService orderService;

    @BeforeEach
    void setUp() {

    }

    @Test
    void saveOrder() {
        UUID orderId = UUID.randomUUID();
        UUID addressId = UUID.randomUUID();
        UUID lineItemId = UUID.randomUUID();
        Order order = new Order();
        order.setId(orderId);
        OrderOutDto orderOutDto = new OrderOutDto();
        orderOutDto.setId(orderId);
        OrderDto orderDto = new OrderDto();
        Address address = new Address();
        address.setId(addressId);
        LineItem lineItem = new LineItem();
        lineItem.setLineItemId(lineItemId);
        List<LineItem> lineItems = new ArrayList<>();
        lineItems.add(lineItem);
        when(orderMapper.toOrder(any())).thenReturn(order);
        when(addressMapper.toAddress(any())).thenReturn(address);
        when(lineItemMapper.toLineItems(anyList())).thenReturn(lineItems);
        when(orderOutMapper.orderToOutDto(any())).thenReturn(orderOutDto);
        OrderOutDto returnOrderOutDto = orderService.saveOrder(orderDto);
        assertNotNull(returnOrderOutDto);
        verify(orderMapper).toOrder(any());
        verify(addressMapper).toAddress(any());
        verify(lineItemMapper).toLineItems(anyList());
        verify(orderRepository, times(1)).save(any());
    }

    @Test
    void deleteOrder() {
        UUID orderId = UUID.randomUUID();
        Order order = new Order();
        when(orderRepository.findById(any())).thenReturn(java.util.Optional.of(order));
        orderService.deleteOrder(orderId);
        verify(orderRepository, times(1)).deleteById(any());
    }

    @Test
    void getOrderById() {
        UUID orderId = UUID.randomUUID();
        Order returnOrder = new Order();
        returnOrder.setId(orderId);
        OrderOutDto returnOrderDtoOut = new OrderOutDto();
        returnOrderDtoOut.setId(orderId);
        when(orderRepository.findById(any())).thenReturn(java.util.Optional.of(returnOrder));
        when(orderOutMapper.orderToOutDto(any())).thenReturn(returnOrderDtoOut);
        OrderOutDto order = orderService.getOrderById(orderId);
        assertEquals(orderId, order.getId());
        verify(orderRepository).findById(any());
        verify(orderOutMapper).orderToOutDto(any());
    }

    @Test
    void getAllOrders() {
        List<Order> returnOrders = new ArrayList<>();
        List<OrderOutDto> returnOrdersOurDto = new ArrayList<>();
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        Order returnOrder1 = new Order();
        Order returnOrder2 = new Order();
        returnOrder1.setId(id1);
        returnOrder2.setId(id2);
        returnOrders.add(returnOrder1);
        returnOrders.add(returnOrder2);
        OrderOutDto returnOrderOut1 = new OrderOutDto();
        OrderOutDto returnOrderOut2 = new OrderOutDto();
        returnOrder1.setId(id1);
        returnOrder2.setId(id2);
        returnOrdersOurDto.add(returnOrderOut1);
        returnOrdersOurDto.add(returnOrderOut2);
        when(orderRepository.findAll()).thenReturn(returnOrders);
        when(orderOutMapper.toOrdersOutDto(any())).thenReturn(returnOrdersOurDto);
        List<OrderOutDto> orders = orderService.getAllOrders();
        assertNotNull(orders);
        assertEquals(2,orders.size());
        verify(orderRepository).findAll();
        verify(orderOutMapper).toOrdersOutDto(any());
    }

    @Test
    void updateOrderWithOnlyOneLineItem() {
        UUID orderId = UUID.randomUUID();
        UUID lineItemId = UUID.randomUUID();
        UUID productId = UUID.randomUUID();
        Order returnOrder = new Order();
        OrderPatchDto orderPatchDto = new OrderPatchDto();
        orderPatchDto.setId(orderId);
        returnOrder.setId(orderId);
        LineItemDto lineItemDto = new LineItemDto();
        List<LineItemDto> lineItems = new ArrayList<>();
        lineItemDto.setProductId(productId);
        lineItemDto.setQty(5);
        lineItems.add(lineItemDto);
        orderPatchDto.setLineItems(lineItems);
        when(orderRepository.findById(any())).thenReturn(java.util.Optional.of(returnOrder));
        OrderOutDto orderOutDto = orderService.updateOrder(orderPatchDto, orderId);
        verify(orderPatchMapper, times(1)).updateOrderFromPatchDto(any(), any());
    }

    @Test
    void updateOrderWithTwoLineItems() {
        UUID orderId = UUID.randomUUID();
        UUID lineItemId = UUID.randomUUID();
        UUID productId = UUID.randomUUID();
        UUID productId2 = UUID.randomUUID();
        Order returnOrder = new Order();
        OrderPatchDto orderPatchDto = new OrderPatchDto();
        orderPatchDto.setId(orderId);
        returnOrder.setId(orderId);
        LineItemDto lineItemDto = new LineItemDto();
        LineItem lineItem = new LineItem();
        List<LineItemDto> lineItemDtos = new ArrayList<>();
        List<LineItem> lineItems = new ArrayList<>();
        lineItemDto.setProductId(productId);
        lineItemDto.setQty(5);
        lineItem.setProductId(productId2);
        lineItem.setQty(10);
        lineItemDtos.add(lineItemDto);
        lineItems.add(lineItem);
        orderPatchDto.setLineItems(lineItemDtos);
        returnOrder.setLineItems(lineItems);
        when(orderRepository.findById(any())).thenReturn(java.util.Optional.of(returnOrder));
        OrderOutDto orderOutDto = orderService.updateOrder(orderPatchDto, orderId);
        verify(orderPatchMapper, times(1)).updateOrderFromPatchDto(any(), any());
    }
}