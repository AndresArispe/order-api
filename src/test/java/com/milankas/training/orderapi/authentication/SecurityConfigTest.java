package com.milankas.training.orderapi.authentication;

import com.milankas.training.orderapi.dto.out.OrderOutDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class SecurityConfigTest {

    @Autowired
    private TestRestTemplate template;

    @BeforeEach
    void setUp() {

    }

    @Test
    void configureGlobalNotFound() {
        ResponseEntity<OrderOutDto> result = template.withBasicAuth("order", "orders.123")
                .getForEntity("/v1/orders/c3c2e1cc-b55d-43d8-b389-6da56bec950b", OrderOutDto.class);
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }

}