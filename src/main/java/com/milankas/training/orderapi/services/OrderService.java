package com.milankas.training.orderapi.services;

import com.milankas.training.orderapi.dto.in.OrderDto;
import com.milankas.training.orderapi.dto.in.patch.OrderPatchDto;
import com.milankas.training.orderapi.dto.log.LogStatusDto;
import com.milankas.training.orderapi.dto.out.OrderOutDto;
import com.milankas.training.orderapi.mappers.in.AddressMapper;
import com.milankas.training.orderapi.mappers.in.LineItemMapper;
import com.milankas.training.orderapi.mappers.in.OrderMapper;
import com.milankas.training.orderapi.mappers.in.patch.OrderPatchMapper;
import com.milankas.training.orderapi.mappers.out.OrderOutMapper;
import com.milankas.training.orderapi.persistance.model.Address;
import com.milankas.training.orderapi.persistance.model.LineItem;
import com.milankas.training.orderapi.persistance.model.Order;
import com.milankas.training.orderapi.persistance.repository.AddressRepository;
import com.milankas.training.orderapi.persistance.repository.LineItemRepository;
import com.milankas.training.orderapi.persistance.repository.OrderRepository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class OrderService {

    private AddressRepository addressRepository;
    private LineItemRepository lineItemRepository;
    private OrderRepository orderRepository;
    private OrderMapper orderMapper;
    private LineItemMapper lineItemMapper;
    private AddressMapper addressMapper;
    private OrderOutMapper orderOutMapper;
    private OrderPatchMapper orderPatchMapper;

    public OrderService(OrderPatchMapper orderPatchMapper, AddressRepository addressRepository, LineItemRepository lineItemRepository, OrderRepository orderRepository, OrderMapper orderMapper, LineItemMapper lineItemMapper, AddressMapper addressMapper, OrderOutMapper orderOutMapper) {
        this.addressRepository = addressRepository;
        this.lineItemRepository = lineItemRepository;
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
        this.lineItemMapper = lineItemMapper;
        this.addressMapper = addressMapper;
        this.orderOutMapper = orderOutMapper;
        this.orderPatchMapper = orderPatchMapper;
    }

    public OrderOutDto saveOrder(OrderDto orderDto) {
        Order newOrder = orderMapper.toOrder(orderDto);
        Address newAddress = addressMapper.toAddress(orderDto.getAddress());
        newAddress.setOrder(newOrder);
        newOrder.setAddress(newAddress);
        List<LineItem> newLineItems = lineItemMapper.toLineItems(orderDto.getLineItems());
        for(int i = 0; i < newLineItems.size(); i++) {
            newLineItems.get(i).setOrder(newOrder);
        }
        newOrder.setLineItems(newLineItems);
        orderRepository.save(newOrder);
        return orderOutMapper.orderToOutDto(newOrder);
    }

    public OrderOutDto deleteOrder(UUID id) {
        Order deletedOrder = orderRepository.findById(id).orElse(null);
        if (deletedOrder == null) {
            return null;
        }
        else {
            orderRepository.deleteById(id);
            return orderOutMapper.orderToOutDto(deletedOrder);
        }
    }

    public OrderOutDto getOrderById(UUID id) {
        return orderOutMapper.orderToOutDto(orderRepository.findById(id).orElse(null));
    }

    public List<OrderOutDto> getAllOrders() {
        return orderOutMapper.toOrdersOutDto(orderRepository.findAll());
    }

    public OrderOutDto updateOrder(OrderPatchDto orderPatchDto, UUID orderId) {
        Order existingOrder = orderRepository.findById(orderId).orElse(null);
        if (existingOrder == null) {
            return null;
        }
        else {
            if (orderPatchDto.getLineItems().size() < 1) {
                orderPatchDto.setLineItems(lineItemMapper.toLineItemsDto(existingOrder.getLineItems()));
                for(int i = (existingOrder.getLineItems().size() - 1); i >=0; i--) {
                    UUID id = existingOrder.getLineItems().get(i).getLineItemId();
                    existingOrder.getLineItems().remove(i);
                    lineItemRepository.deleteById(id);
                }
                orderPatchMapper.updateOrderFromPatchDto(orderPatchDto, existingOrder);
            }
            else {
                for(int i = (existingOrder.getLineItems().size() - 1); i >=0; i--) {
                    UUID id = existingOrder.getLineItems().get(i).getLineItemId();
                    existingOrder.getLineItems().remove(i);
                    lineItemRepository.deleteById(id);
                }
                orderPatchMapper.updateOrderFromPatchDto(orderPatchDto, existingOrder);
            }
        }
        return orderOutMapper.orderToOutDto(orderRepository.save(existingOrder));
    }

    public LogStatusDto buildLogStatusDto(String level, String message) {
        LogStatusDto logStatusDto = new LogStatusDto();
        logStatusDto.setMessage(message);
        logStatusDto.setLevel(level);
        logStatusDto.setDateAndTime(new Date());
        logStatusDto.setServiceName("Order-API");
        return logStatusDto;
    }
}
