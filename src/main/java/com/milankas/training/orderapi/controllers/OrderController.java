package com.milankas.training.orderapi.controllers;

import com.milankas.training.orderapi.dto.in.OrderDto;
import com.milankas.training.orderapi.dto.in.patch.OrderPatchDto;
import com.milankas.training.orderapi.dto.log.LogStatusDto;
import com.milankas.training.orderapi.dto.out.OrderOutDto;
import com.milankas.training.orderapi.errors.ErrorResponse;
import com.milankas.training.orderapi.message.MessagingConfig;
import com.milankas.training.orderapi.services.OrderService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class OrderController {

    private LogStatusDto logStatusDto = new LogStatusDto();

    private OrderService orderService;
    private RabbitTemplate template;

    public OrderController(OrderService orderService, RabbitTemplate template) {
        this.orderService = orderService;
        this.template = template;
    }

    @GetMapping("/v1/orders")
    public List<OrderOutDto> getAllOrders() {
        logStatusDto = orderService.buildLogStatusDto("info", "get orders list");
        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
        return orderService.getAllOrders();
    }

    @GetMapping("/v1/orders/{orderId}")
    public OrderOutDto getOrder(@PathVariable UUID orderId) {
        if (orderService.getOrderById(orderId) == null) {
            logStatusDto = orderService.buildLogStatusDto("error", "get order by id");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "order not found"
            );
        }
        else {
            logStatusDto = orderService.buildLogStatusDto("info", "get order by id");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return orderService.getOrderById(orderId);
        }
    }

    @GetMapping("/v1/orders/healthcheck")
    public String healthCheck() {
        logStatusDto = orderService.buildLogStatusDto("info", "get healthcheck");
        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
        return "I'm Alive";
    }

    @PostMapping("/v1/orders")
    public ResponseEntity<Object> postOrder(@Valid @RequestBody OrderDto orderDto, BindingResult result) {
        if (result.hasErrors()) {
            List<String> errors = result.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());
            logStatusDto = orderService.buildLogStatusDto("error", "adding new order");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return ResponseEntity.badRequest().body(new ErrorResponse("400", "Validation Failure", errors));
        }
        else {
            logStatusDto = orderService.buildLogStatusDto("info", "adding new order");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return new ResponseEntity<>(orderService.saveOrder(orderDto), HttpStatus.CREATED);
        }
    }

    @DeleteMapping("/v1/orders/{orderId}")
    public ResponseEntity deleteOrder(@PathVariable UUID orderId) {
        if (orderService.deleteOrder(orderId) == null) {
            logStatusDto = orderService.buildLogStatusDto("error", "deleting order");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "order not found"
            );
        }
        else {
            logStatusDto = orderService.buildLogStatusDto("info", "deleting order");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @PatchMapping("/v1/orders/{orderId}")
    public ResponseEntity<Object> patchOrder(@PathVariable UUID orderId,@Valid @RequestBody OrderPatchDto orderPatchDto, BindingResult result) {
        if (result.hasErrors()) {
            List<String> errors = result.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());
            logStatusDto = orderService.buildLogStatusDto("error", "updating order");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return ResponseEntity.badRequest().body(new ErrorResponse("400", "Validation Failure", errors));
        }
        else {
            if (orderService.getOrderById(orderId) == null) {
                logStatusDto = orderService.buildLogStatusDto("error", "updating order");
                template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
                return ResponseEntity.notFound().build();
            }
            else {
                logStatusDto = orderService.buildLogStatusDto("info", "updating order");
                template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
                return ResponseEntity.ok(orderService.updateOrder(orderPatchDto, orderId));
            }
        }
    }
}
