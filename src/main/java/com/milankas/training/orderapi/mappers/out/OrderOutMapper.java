package com.milankas.training.orderapi.mappers.out;

import com.milankas.training.orderapi.dto.in.OrderDto;
import com.milankas.training.orderapi.dto.out.OrderOutDto;
import com.milankas.training.orderapi.persistance.model.Order;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderOutMapper {

    OrderOutDto orderToOutDto(Order order);

    List<OrderOutDto> toOrdersOutDto(List<Order> orders);

    Order toOrder(OrderOutDto orderOutDto);
}
