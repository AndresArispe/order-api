package com.milankas.training.orderapi.mappers.in.patch;

import com.milankas.training.orderapi.dto.in.patch.OrderPatchDto;
import com.milankas.training.orderapi.persistance.model.Order;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;


@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface OrderPatchMapper {

    void updateOrderFromPatchDto(OrderPatchDto orderPatchDto, @MappingTarget Order entity);

}
