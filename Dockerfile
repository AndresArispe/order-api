FROM openjdk:8-jdk-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
ENV APP_PORT=8091
ENV DB_URL=jdbc:postgresql://postgres-orders:5432/order
ENV DB_USER=postgres
ENV DB_PASSWORD=godis1first
ARG JAR_FILE=target/*.jar
ADD ${JAR_FILE} api.jar
ENTRYPOINT ["java","-Dspring.profiles.active=docker","-jar","api.jar"]